# Les débuts du jeu viéo 

Les jeux vidéo ont été imaginés dès les années 1950 par Ralph Baer.

Le tout premier jeu vidéo est créé en 1952 par A.S Douglas. Le nom du jeu, OXO, reflète son contenu : c’est un jeu de morpion.

En 1958, Willy Higinbotham crée Tennis for two, qui simule une partie de tennis sur un ordinateur relié à un oscilloscope servant d'écran.

En 1961, le jeu Spacewar est créé par des étudiants. Il met en scène deux vaisseaux qui s’affrontent, contrôlés par deux joueurs équipés de manettes.